westeros = {
    'north': {
        'house': 'stark',
        'army': 15,
        'close_to': ['riverlands']
    },
    'riverlands': {
        'house': 'tully',
        'army': 13,
        'close_to': ['vale', 'crownlands', 'westerlands', 'reach', 'north']
    },
    'reach': {
        'house': 'tyrell',
        'army': 14,
        'close_to': ['crownlands', 'westerlands', 'reach', 'north', 'dorne', 'stormlands', 'riverlands']
    },
    'vale': {
        'house': 'arryn',
        'army': 12,
        'close_to': ['riverlands']
    },
    'westerlands': {
        'house': 'lannister',
        'army': 11,
        'close_to': ['riverlands', 'reach']
    },
    'crownlands': {
        'house': 'targaryen',
        'army': 10,
        'close_to': ['riverlands', 'reach', 'stormlands']
    },
    'dorne': {
        'house': 'martell',
        'army': 12,
        'close_to': ['reach', 'stormlands']
    },
    'stormlands': {
        'house': 'baratheon',
        'army': 11,
        'close_to': ['crownlands', 'reach', 'dorne']
    }
}
