from app.risk.dice import pick_kingdom
from app.ports.players import Players


class AutoPlayers(Players):
    def next_move(self, current_player):
        return pick_kingdom(list(set(self.kingdoms) - set(current_player)))
