from app.ports.history import History


class BookHistory(History):
    def record_attacker(self, house, kingdom):
        with open('history.log', 'a') as f:
            f.write(self.attacker_log % (house, kingdom))

    def record_defender(self, house, kingdom):
        with open('history.log', 'a') as f:
            f.write(self.defender_log % (house, kingdom))

    def record_victory(self, house, kingdom):
        with open('history.log', 'a') as f:
            f.write(self.victory_log % (house, kingdom))

    def record_impossible_battle(self, error):
        with open('history.log', 'a') as f:
            f.write(self.impossible_battle_log % error)
