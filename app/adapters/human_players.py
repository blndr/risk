from app.ports.players import Players


class HumanPlayers(Players):
    def next_move(self, current_player):
        return input('\n> %s is playing. Where to attack next ?\n\t > ' % current_player)
