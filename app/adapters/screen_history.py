from app.ports.history import History


class ScreenHistory(History):
    def record_attacker(self, house, kingdom):
        print(self.attacker_log % (house, kingdom))

    def record_defender(self, house, kingdom):
        print(self.defender_log % (house, kingdom))

    def record_victory(self, house, kingdom):
        print(self.victory_log % (house, kingdom))

    def record_impossible_battle(self, error):
        print(self.impossible_battle_log % error)
