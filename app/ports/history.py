from abc import ABC, abstractmethod


class History(ABC):
    def __init__(self):
        self.attacker_log = '\nHouse %s is attacking from %s'
        self.defender_log = '\nHouse %s is defending %s '
        self.victory_log = '\nHouse %s won the battle ! %s belongs to them'
        self.impossible_battle_log = '\nImpossible battle ! Because %s'

    @abstractmethod
    def record_attacker(self, house, kingdom):
        pass

    @abstractmethod
    def record_defender(self, house, kingdom):
        pass

    @abstractmethod
    def record_victory(self, house, kingdom):
        pass

    @abstractmethod
    def record_impossible_battle(self, error):
        pass
