from abc import ABC, abstractmethod
from itertools import cycle


class Players(ABC):
    def __init__(self, kingdoms):
        self.kingdoms = kingdoms
        self.kingdoms_board = cycle(kingdoms)

    def next_player(self):
        return next(self.kingdoms_board)

    @abstractmethod
    def next_move(self, current_player):
        pass
