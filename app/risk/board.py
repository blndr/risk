class Board:
    def __init__(self, map):
        self.map = map

    def owner(self, kingdom):
        return self.map[kingdom]['house']

    def battalions(self, kingdom):
        return self.map[kingdom]['army']

    def neighbours(self, kingdom):
        return self.map[kingdom]['close_to']

    def controls(self, kingdom, house):
        self.map[kingdom]['house'] = house

    def garrisons(self, kingdom, battalions):
        self.map[kingdom]['army'] = battalions

    def recruitment(self):
        for kingdom, household in self.map.items():
            household['army'] += 1

    def kingdoms(self):
        return list(self.map.keys())
