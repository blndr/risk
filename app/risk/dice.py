from random import choice


def defense_bonus():
    return choice([0, 1]) == 1


def attack_malus():
    return choice([-1, 0, 0]) == -1


def pick_kingdom(kingdoms):
    return choice(kingdoms)
