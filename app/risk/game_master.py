from app.ports.players import Players

from app.ports.history import History
from app.risk.truce import ImpossibleBattle
from app.risk.war import War


class GameMaster:
    def __init__(self, war: War, players: Players, history: History):
        self.war = war
        self.players = players
        self.history = history

    def play(self):
        attacker = self.players.next_player()
        defender = self.players.next_move(attacker)

        self.history.record_attacker(self.war.owner(attacker), attacker)
        self.history.record_defender(self.war.owner(defender), defender)
        try:
            self.war.declaration(attacker, defender)
        except ImpossibleBattle as e:
            self.history.record_impossible_battle(e.error.value)
        else:
            winner = self.war.battle(attacker, defender)
            if winner == attacker:
                self.war.attacker_victory(attacker, defender)
            else:
                self.war.defender_victory(attacker, defender)
            self.history.record_victory(self.war.owner(defender), winner)
