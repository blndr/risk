from enum import Enum


class ImpossibleBattle(Exception):
    def __init__(self, error):
        self.error = error


class BattleError(Enum):
    KINGDOM_TOO_FAR_AWAY = 'opponent kingdoms are too far away.',
    SAME_HOUSE = 'one cannot attack the same house.'
    ARMY_TOO_SMALL = 'there is not enough battalions to attack.'
