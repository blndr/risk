from app.risk.dice import attack_malus, defense_bonus
from app.risk.truce import BattleError, ImpossibleBattle

VICTORY_BONUS = 1
MIN_ARMY = 2


class War:
    def __init__(self, board):
        self.board = board

    def battle(self, attacker, defender):
        attacker_army = self._prepare_army(attacker)
        defender_army = self._prepare_army(defender, is_defender=True)
        if attacker_army + attack_malus() > defender_army + defense_bonus():
            return attacker
        else:
            return defender

    def attacker_victory(self, attacker, defender):
        attacker_army = self._prepare_army(attacker)
        self.board.controls(defender, self.board.owner(attacker))
        self.board.garrisons(defender, attacker_army + VICTORY_BONUS)
        self.board.garrisons(attacker, self.board.battalions(attacker) - attacker_army)

    def defender_victory(self, attacker, defender):
        attacker_army = self._prepare_army(attacker)
        self.board.garrisons(attacker, self.board.battalions(attacker) - attacker_army)
        self.board.garrisons(defender, self.board.battalions(defender) + VICTORY_BONUS)

    def declaration(self, attacker, defender):
        if defender not in self.board.neighbours(attacker):
            raise ImpossibleBattle(BattleError.KINGDOM_TOO_FAR_AWAY)
        if self.board.owner(attacker) == self.board.owner(defender):
            raise ImpossibleBattle(BattleError.SAME_HOUSE)
        if self.board.battalions(attacker) < MIN_ARMY:
            raise ImpossibleBattle(BattleError.ARMY_TOO_SMALL)

    def owner(self, kingdom):
        return self.board.owner(kingdom)

    def recruitment(self):
        self.board.recruitment()

    def _prepare_army(self, kingdom, is_defender=False):
        army = self.board.battalions(kingdom) // 2
        if is_defender:
            army += self.board.battalions(kingdom) % 2
        return army
