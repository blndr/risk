#!/usr/bin/python3
from time import sleep

from app import westeros
from app.adapters.auto_players import AutoPlayers
from app.adapters.screen_history import ScreenHistory
from app.risk.board import Board
from app.risk.game_master import GameMaster
from app.risk.war import War

board = Board(westeros)

game_master = GameMaster(
    War(board),
    AutoPlayers(board.kingdoms()),
    ScreenHistory()
)

while True:
    sleep(1)
    game_master.play()
