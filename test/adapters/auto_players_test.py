from unittest import TestCase
from unittest.mock import patch

from app.adapters.auto_players import AutoPlayers


@patch('app.adapters.auto_players.pick_kingdom')
class AutoPlayerTest(TestCase):
    def test_pick_kingdom_from_all_kingdoms_except_current_player(self, pick_kingdom):
        # given
        players = AutoPlayers(['north', 'reach', 'dorne'])

        # when
        players.next_move('reach')

        # then
        pick_kingdom.assert_called_with == ['north', 'dorne']

    def test_next_move_returns_another_kingdom(self, pick_kingdom):
        # given
        players = AutoPlayers(['north', 'reach', 'dorne'])
        pick_kingdom.return_value = 'dorne'

        # when
        next_move = players.next_move('reach')

        # then
        assert next_move == 'dorne'
