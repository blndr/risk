from unittest import TestCase

from app.ports.players import Players


class TestablePlayers(Players):
    def next_move(self):
        pass


class PlayersTest(TestCase):
    def test_next_player_returns_the_first_kingdom(self):
        # given
        players = TestablePlayers(['north', 'reach', 'dorne'])

        # when
        next_player = players.next_player()

        # then
        assert next_player == 'north'

    def test_next_player_returns_the_second_player_at_second_move(self):
        # given
        players = TestablePlayers(['north', 'reach', 'dorne'])
        players.next_player()

        # when
        next_player = players.next_player()

        # then
        assert next_player == 'reach'

    def test_next_player_cycle_back_to_the_first_kingdom_when_the_last_has_played(self):
        # given
        players = TestablePlayers(['north', 'reach', 'dorne'])
        players.next_player()
        players.next_player()
        players.next_player()

        # when
        next_player = players.next_player()

        # then
        assert next_player == 'north'
