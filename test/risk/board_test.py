from app.risk.board import Board


class BoardTest:
    def setup_method(self, method):
        self.board = Board({
            'north': {'house': 'stark', 'army': 12, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 7, 'close_to': ['north']}
        })

    def test_returns_the_house_owning_given_kingdom(self):
        # when
        house = self.board.owner('north')

        # then
        assert house == 'stark'

    def test_returns_the_army_stationed_on_given_kingdom(self):
        # when
        battalions = self.board.battalions('north')

        # then
        assert battalions == 12

    def test_returns_the_neighbours_of_given_kingdom(self):
        # when
        neighbours = self.board.neighbours('north')

        # then
        assert neighbours == ['riverlands']

    def test_a_house_takes_control_of_a_kingdom(self):
        # when
        self.board.controls('north', 'tully')

        # then
        assert self.board.owner('north') == 'tully'

    def test_a_kingdom_stations_some_battalions(self):
        # when
        self.board.garrisons('north', 6)

        # then
        assert self.board.battalions('north') == 6

    def test_recruitment_adds_one_battalion_to_every_kingdom(self):
        # when
        self.board.recruitment()

        # then
        assert self.board.battalions('north') == 13
        assert self.board.battalions('riverlands') == 8

    def test_kingdoms_return_the_list_of_kingdoms_on_the_board(self):
        # when
        kingdoms = self.board.kingdoms()

        # then
        assert 'riverlands' and 'north' in kingdoms
