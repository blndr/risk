from unittest.mock import Mock

from app.ports.history import History
from app.ports.players import Players
from app.risk.game_master import GameMaster
from app.risk.truce import BattleError, ImpossibleBattle
from app.risk.war import War


class GameMasterTest:
    def setup_method(self, method):
        self.war = Mock(spec=War)
        self.players = Mock(spec=Players)
        self.history = Mock(spec=History)
        self.game_master = GameMaster(self.war, self.players, self.history)

    def test_runs_battle_with_next_attacker_and_defender(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'

        # when
        self.game_master.play()

        # then
        self.war.battle.assert_called_with('vale', 'crownlands')

    def test_records_in_history_who_is_attacking_from_where(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'
        self.war.owner.return_value = 'arryn'

        # when
        self.game_master.play()

        # then
        self.history.record_attacker.assert_called_with('arryn', 'vale')

    def test_records_in_history_who_is_defending_and_where(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'
        self.war.owner.return_value = 'targaryen'

        # when
        self.game_master.play()

        # then
        self.history.record_defender.assert_called_with('targaryen', 'crownlands')

    def test_does_not_run_battle_if_declaration_of_war_failed(self):
        # given
        self.war.declaration.side_effect = ImpossibleBattle(BattleError.KINGDOM_TOO_FAR_AWAY)

        # when
        self.game_master.play()

        # then
        self.war.battle.assert_not_called()

    def test_records_in_history_if_declaration_of_war_failed(self):
        # given
        self.war.declaration.side_effect = ImpossibleBattle(BattleError.KINGDOM_TOO_FAR_AWAY)

        # when
        self.game_master.play()

        # then
        self.history.record_impossible_battle(BattleError.KINGDOM_TOO_FAR_AWAY.value)

    def test_apply_attacker_victory_after_battle_if_attacker_won(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'
        self.war.battle.return_value = 'vale'

        # when
        self.game_master.play()

        # then
        self.war.attacker_victory.assert_called_with('vale', 'crownlands')

    def test_apply_defender_victory_after_battle_if_attacker_lost(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'
        self.war.battle.return_value = 'crownlands'

        # when
        self.game_master.play()

        # then
        self.war.defender_victory.assert_called_with('vale', 'crownlands')

    def test_records_victory_in_history(self):
        # given
        self.players.next_player.return_value = 'vale'
        self.players.next_move.return_value = 'crownlands'
        self.war.battle.return_value = 'crownlands'
        self.war.owner.return_value = 'targaryen'

        # when
        self.game_master.play()

        # then
        self.history.record_victory.assert_called_with('targaryen', 'crownlands')
