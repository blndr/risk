from unittest import TestCase
from unittest.mock import patch

from pytest import raises

from app.risk.board import Board
from app.risk.truce import ImpossibleBattle, BattleError
from app.risk.war import War


@patch('app.risk.war.attack_malus')
@patch('app.risk.war.defense_bonus')
class BattleTest:
    def setup_method(self, method):
        self.war = War(Board({}))

    def test_the_defender_wins_if_same_number_of_battalions_on_both_sides(self, defense_bonus, attack_malus):
        # given
        before_battle = {
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 8, 'close_to': ['north']}
        }
        defense_bonus.return_value = 0
        attack_malus.return_value = 0
        self.war.board = Board(before_battle)

        # when
        winner = self.war.battle('north', 'riverlands')

        # then
        assert winner == 'riverlands'

    def test_the_attacker_wins_if_he_has_more_battalions(self, defense_bonus, attack_malus):
        # given
        before_battle = {
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 4, 'close_to': ['north']}
        }
        defense_bonus.return_value = 0
        attack_malus.return_value = 0
        self.war.board = Board(before_battle)

        # when
        winner = self.war.battle('north', 'riverlands')

        # then
        assert winner == 'north'

    def test_the_defender_has_a_defense_bonus_if_odd_number_of_battalions(self, defense_bonus, attack_malus):
        # given
        before_battle = {
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 9, 'close_to': ['north']}
        }
        defense_bonus.return_value = 0
        attack_malus.return_value = 0
        self.war.board = Board(before_battle)

        # when
        winner = self.war.battle('north', 'riverlands')

        # then
        assert winner == 'riverlands'

    def test_the_attacker_loses_if_he_has_more_battalions_but_suffers_an_attack_malus(self, defense_bonus,
                                                                                      attack_malus):
        # given
        before_battle = {
            'north': {'house': 'stark', 'army': 10, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 7, 'close_to': ['north']}
        }
        defense_bonus.return_value = 0
        attack_malus.return_value = -1
        self.war.board = Board(before_battle)

        # when
        winner = self.war.battle('north', 'riverlands')

        # then
        assert winner == 'riverlands'


class AttackerVictoryTest(TestCase):
    def setup_method(self, method):
        self.war = War(Board({}))

    def test_the_attacker_gains_one_battalion(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 3, 'close_to': ['north']}
        })

        # when
        self.war.attacker_victory('north', 'riverlands')

        # then
        assert self.war.board.map['riverlands']['army'] == 5

    def test_the_attacker_origin_kingdom_keeps_the_remaining_battalions(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 3, 'close_to': ['north']}
        })

        # when
        self.war.attacker_victory('north', 'riverlands')

        # then
        assert self.war.board.map['north']['army'] == 4

    def test_the_attacker_takes_possession_of_defending_kingdom(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 8, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 3, 'close_to': ['north']}
        })

        # when
        self.war.attacker_victory('north', 'riverlands')

        # then
        assert self.war.board.map['riverlands']['house'] == 'stark'


class DefenderVictoryTest(TestCase):
    def setup_method(self, method):
        self.war = War(Board({}))

    def test_the_attacker_loses_its_attacking_battalions(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 2, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 3, 'close_to': ['north']}
        })

        # when
        self.war.defender_victory('north', 'riverlands')

        # then
        assert self.war.board.map['north']['army'] == 1

    def test_the_defender_army_gains_one_battalion(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 2, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 3, 'close_to': ['north']}
        })

        # when
        self.war.defender_victory('north', 'riverlands')

        # then
        assert self.war.board.map['riverlands']['army'] == 4


class DeclarationTest(TestCase):
    def setup_method(self, method):
        self.war = War(Board({}))

    def test_an_attacker_cannot_go_to_battle_with_no_battalions(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 0, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 1, 'close_to': ['north']}
        })

        # when
        with raises(ImpossibleBattle) as e:
            self.war.declaration('north', 'riverlands')

        # then
        assert e.value.error == BattleError.ARMY_TOO_SMALL

    def test_an_attacker_cannot_go_to_battle_with_one_battalion(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 1, 'close_to': ['riverlands']},
            'riverlands': {'house': 'tully', 'army': 1, 'close_to': ['north']}
        })

        # when
        with raises(ImpossibleBattle) as e:
            self.war.declaration('north', 'riverlands')

        # then
        assert e.value.error == BattleError.ARMY_TOO_SMALL

    def test_an_attacker_cannot_go_to_battle_if_he_is_not_adjacent_to_the_defender(self):
        # given
        self.war.board = Board({
            'north': {'house': 'stark', 'army': 9, 'close_to': ['riverlands']},
            'reach': {'house': 'tyrell', 'army': 3, 'close_to': ['dorne']}
        })

        # when
        with raises(ImpossibleBattle) as e:
            self.war.declaration('north', 'reach')

        # then
        assert e.value.error == BattleError.KINGDOM_TOO_FAR_AWAY

    def test_an_attacker_cannot_go_to_battle_with_the_same_house(self):
        # given
        self.war.board = Board({
            'westerlands': {'house': 'lannister', 'army': 9, 'close_to': ['reach']},
            'reach': {'house': 'lannister', 'army': 3, 'close_to': ['westerlands']}
        })

        # when
        with raises(ImpossibleBattle) as e:
            self.war.declaration('westerlands', 'reach')

        # then
        assert e.value.error == BattleError.SAME_HOUSE
